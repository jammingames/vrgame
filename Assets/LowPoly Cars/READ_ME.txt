LowPoly Cars: Buses v1.0

This asset contains a low poly bus which is perfect for mobile games.
The bus comes with a set of 3 variations:

- School Bus
- Public Bus
- Prison Bus

+++++ If you like it, please, rate it! +++++

For more assets, please check out our website: www.mooshoolabs.com

If there is a particular asset you'd like, or an improvement to this pack which you'd like to see, make sure to let us know.