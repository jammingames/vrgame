﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public static class PDF  {

	[DllImport("__Internal")]
	public static extern void OpenPDF(string pdfFilename, bool animated);
}
