//
//  PDFPlugin.m
//  Unity-iPhone
//
//  Created by Andrew Lee on 2015-11-17.
//
//

#import <UIKit/UIKit.h>
#import "PDFViewController.h"

extern UIViewController *UnityGetGLViewController();

extern "C" {
    void OpenPDF(const char* pdfFilename, bool animated) {
        PDFViewController *pvc = [[PDFViewController alloc] init];
        pvc.pdfFilename = [NSString stringWithUTF8String:pdfFilename];
        pvc.animatedPresentation = animated;
        [UnityGetGLViewController() presentViewController:pvc animated:animated completion:NULL];
    }
}