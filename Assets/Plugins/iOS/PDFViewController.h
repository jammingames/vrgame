//
//  PDFViewController.h
//  Unity-iPhone
//
//  Created by Andrew Lee on 2015-11-17.
//
//

#import <UIKit/UIKit.h>

@interface PDFViewController : UIViewController
@property NSString *pdfFilename;
@property BOOL animatedPresentation;
@end
