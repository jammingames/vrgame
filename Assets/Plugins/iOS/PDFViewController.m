//
//  PDFViewController.m
//  Unity-iPhone
//
//  Created by Andrew Lee on 2015-11-17.
//
//

#import "PDFViewController.h"

@interface PDFViewController ()
{
    UIWebView *webView;
    UIButton *closeBtn;
}
@end

const float closeBtnWidth = 100.0f;
const float closeBtnHeight = 40.0f;
const float closeBtnTopPadding = 20.0f;
const float closeBtnRightPadding = 20.0f;

@implementation PDFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:webView];
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [closeBtn setFrame:CGRectMake(self.view.frame.size.width - closeBtnWidth - closeBtnRightPadding, closeBtnTopPadding, closeBtnWidth, closeBtnHeight)];
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(onCloseBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    
    if(_pdfFilename) {
        NSURL *url = [[NSBundle mainBundle] URLForResource:_pdfFilename withExtension:nil];
        if(url) {
            [webView loadRequest:[NSURLRequest requestWithURL:url]];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) onCloseBtnPressed:(UIButton*)sender {
    [self dismissViewControllerAnimated:_animatedPresentation completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
