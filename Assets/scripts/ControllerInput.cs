﻿using UnityEngine;
using System.Collections;

public class ControllerInput : MonoBehaviour {

    SteamVR_TrackedObject trackedObj;

    

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    public SteamVR_TrackedObject GetTracker()
    {
        return trackedObj;
    }

    void FixedUpdate()
    {
		ViveInputEvents.TriggerOnTouch (trackedObj);

    }



}
