﻿using UnityEngine;
using System.Collections;

public class HandleInputs : MonoBehaviour {

	public SteamVR_TrackedObject trackedObj;

    IUserInput inputObj;

    void Awake()
    {
        inputObj = GetComponent<IUserInput>();
    }

    void OnEnable()
    {
        ViveInputEvents.OnTouch += HandleOnTouch;
    }

    void OnDisable()
    {
        ViveInputEvents.OnTouch -= HandleOnTouch;
    }

    public void HandleOnTouch(SteamVR_TrackedObject args)
    {
		if (args == trackedObj)
		{
			inputObj.HandleInput(args);
		}
    }

}
