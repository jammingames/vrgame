﻿

public interface IUserInput {

	void HandleInput(SteamVR_TrackedObject trackedObj);
}
