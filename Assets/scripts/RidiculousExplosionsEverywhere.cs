﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RidiculousExplosionsEverywhere : MonoBehaviour {

	#region variables
	public float delayBetweenSpawns = 0.05f;
	public float durationOfSpawns = 8;
	public float spawnLifetime = 5;
	public Transform spawnReference;
	public GameObject[] spawnObjects;


	private float elapsed = 0;
	private float t = 0;
	#endregion


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.U))
		{
			StartCoroutine (SpawnObjects ());		
		}

	}
		

	#region eventHandlers



	#endregion

	#region public methods
	public IEnumerator SpawnObjects()
	{
		while (elapsed <= durationOfSpawns)
		{
			elapsed = Mathf.MoveTowards (elapsed, durationOfSpawns, Time.deltaTime);
			for (int i = 0; i < spawnObjects.Length; i++)
			{
				GameObject gObj = GameObject.Instantiate (spawnObjects [i], spawnReference.position, spawnReference.rotation) as GameObject;
				if (gObj.GetComponent<DestroyAfterDelay> () == null)
					gObj.AddComponent<DestroyAfterDelay> ();
				gObj.GetComponent<DestroyAfterDelay> ().delay = spawnLifetime;
			}
			yield return 0;
		}
	}

	#endregion


}
