﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public static class InputVR {



	public static ulong trigger = SteamVR_Controller.ButtonMask.Trigger;
	public static ulong system = SteamVR_Controller.ButtonMask.System;
	public static ulong applicationMenu = SteamVR_Controller.ButtonMask.ApplicationMenu;
	public static ulong grip = SteamVR_Controller.ButtonMask.Grip;
	public static ulong touchpad = SteamVR_Controller.ButtonMask.Touchpad;

	public static SteamVR_Controller.Device GetDevice(SteamVR_TrackedObject trackedObj)
	{
		return SteamVR_Controller.Input ((int)trackedObj.index);
	}

	public static bool GetTouch (SteamVR_Controller.Device device, ulong mask)
	{
		return device.GetTouch (mask);
	}

	public static bool GetTouchDown (SteamVR_Controller.Device device, ulong mask)
	{
		return device.GetTouchDown (mask);
	}

	public static bool GetTouchUp (SteamVR_Controller.Device device, ulong mask)
	{
		return device.GetTouchUp (mask);
	}

	public static bool GetPress (SteamVR_Controller.Device device, ulong mask)
	{
		return device.GetPress (mask);
	}

	public static bool GetPressDown (SteamVR_Controller.Device device, ulong mask)
	{
		return device.GetPressDown (mask);
	}

	public static bool GetPressUp (SteamVR_Controller.Device device, ulong mask)
	{
		return device.GetPressUp(mask);
	}






}

public static class ViveInputEvents {

	public delegate void InputEvent (SteamVR_TrackedObject trackedObj);

    public static event InputEvent OnTouch, OnTouchDown, OnTouchUp;

    public static SteamVR_TrackedObject trackedObj;
    
	public static void TriggerOnTouch(SteamVR_TrackedObject trackedObj)
    {
        if (OnTouch != null)
        {
            OnTouch(trackedObj);
        }
    }

	public static void TriggerOnTouchDown(SteamVR_TrackedObject trackedObj)
    {
        if (OnTouchDown != null)
        {
            OnTouchDown(trackedObj);
        }
    }

	public static void TriggerOnTouchUp(SteamVR_TrackedObject trackedObj)
    {
        if (OnTouchUp != null)
        {
            OnTouchUp(trackedObj);
        }
    }




}
