﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerMovement : MonoBehaviour, IUserInput {

    public SteamVR_TrackedObject leftInput, rightInput;
    Rigidbody rb;
    Vector3 tempVector;
    public Vector3 ThrustDirection = Vector3.zero;
    public float ThrustForce = 10;
    FirstPersonController player;

    void Awake()
    {
        player = GetComponent<FirstPersonController>();
        rb = GetComponent<Rigidbody>();
    }
	
    public void HandleInput(ViveInputArgs args)
    {
        if (args.mask == SteamVR_Controller.ButtonMask.Trigger && args.trackedObj == leftInput)
        {
  
            player.SetInput(new Vector2(0, 1));
            //tempVector = Quaternion.Euler(ThrustDirection) * Vector3.forward;
            //rb.AddForce(transform.rotation * tempVector * ThrustForce);
            //rb.maxAngularVelocity = 2f;
        }
      

        if (args.mask == SteamVR_Controller.ButtonMask.Grip && args.trackedObj == leftInput)
        {
            player.SetRun(true);
        }

        if (args.mask == SteamVR_Controller.ButtonMask.ApplicationMenu && args.trackedObj == leftInput)
        {
            Transform c = Camera.main.transform;
            float angle = -4;
                print("GOING LEFT");
            transform.Rotate(0, angle, 0);
            //player.SetRotate(true);
        }

        if (args.mask == SteamVR_Controller.ButtonMask.ApplicationMenu && args.trackedObj == rightInput)
        {
            Transform c = Camera.main.transform;
            float angle = 4;
            print("GOING RIGHT");
            transform.Rotate(0, angle, 0);
        }

    }

    public void HandleInputDown(ViveInputArgs args)
    {
        if (args.mask == SteamVR_Controller.ButtonMask.Trigger && args.trackedObj == leftInput)
        {
//
        }


    }

    public void HandleInputUp(ViveInputArgs args)
    {
        if (args.mask == SteamVR_Controller.ButtonMask.Trigger && args.trackedObj == leftInput)
        {
            player.SetInput(new Vector2(0, 0));
        }

        if (args.mask == SteamVR_Controller.ButtonMask.Touchpad && args.trackedObj == leftInput)
        {
            player.SetJump(true);
        }

        if (args.mask == SteamVR_Controller.ButtonMask.Grip && args.trackedObj == leftInput)
        {
            player.SetRun(false);
        }

      
    }

}
