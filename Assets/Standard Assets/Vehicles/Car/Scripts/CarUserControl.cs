using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

namespace UnityStandardAssets.Vehicles.Car
{


    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {

		public List<float> hArray;
		public List<float> vArray;
		public List<float> handbrakeArray;
        private CarController m_Car; // the car controller we want to use
		int i;

		Vector3 origPos;
		Quaternion origRot;
		Rigidbody rb;

        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
			rb = GetComponent<Rigidbody>();
			origPos = transform.position;
			origRot = transform.rotation;

        }

		void OnEnable()
		{
			i = 0;
		}


		public void Reset()
		{
			rb.isKinematic = true;
			transform.position = origPos;
			transform.rotation = origRot;
			rb.isKinematic = false;
			enabled = false;
		}


        private void FixedUpdate()
        {
            // pass the input to the car!
//            float h = CrossPlatformInputManager.GetAxis("Horizontal");
//            float v = CrossPlatformInputManager.GetAxis("Vertical");
			float h = hArray[i];
			float v = vArray[i];

#if !MOBILE_INPUT
//            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
			float handbrake = handbrakeArray[i];
			if (i < hArray.Count-1)
				i++;
			else
				i = 0;
//			hArray.Add(h);
//			vArray.Add(v);
//			handbrakeArray.Add(handbrake);
//			D.log (v);
            m_Car.Move(h/4, v, v, handbrake);

#else
            m_Car.Move(h, 1, 0, 0f);
			float handbrake = 0;
			if (Input.GetMouseButtonDown(0))
			{
				handbrake = 1;
			}
			else if (!Input.GetMouseButtonDown(0))
			{
				handbrake = 0;
			}
#endif
        }
    }

}