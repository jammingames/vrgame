using System;
using UnityEngine;

namespace UnityStandardAssets.Vehicles.Car
{
    public class BrakeLight : MonoBehaviour
    {
        public CarController car; // reference to the car controller, must be dragged in inspector

        private Renderer m_Renderer;
		private Light[] lights;


        private void Start()
        {
            m_Renderer = GetComponent<Renderer>();
			lights = GetComponentsInChildren<Light>();
        }


        private void Update()
        {
            // enable the Renderer when the car is braking, disable it otherwise.
			for (int i = 0; i < lights.Length; i++)
			{
				if (car.BrakeInput >0f || car.CurrentSpeed < 0.7f)
					lights[i].enabled = true;
				else lights[i].enabled = false;
			}
//            m_Renderer.enabled = car.BrakeInput > 0f;
        }
    }
}
