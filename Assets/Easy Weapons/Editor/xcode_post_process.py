import os
import sys
from sys import argv
from mod_pbxproj import XcodeProject

path = argv[1]
pdf = argv[2]

print('opening project: ' + path + '/Unity-iPhone.xcodeproj/project.pbxproj')

project = XcodeProject.Load(path + '/Unity-iPhone.xcodeproj/project.pbxproj')

print('adding frameworks')
project.add_file_if_doesnt_exist('usr/lib/libsqlite3.tbd', tree='SDKROOT')
project.add_file_if_doesnt_exist('usr/lib/libz.tbd', tree='SDKROOT')
project.add_file_if_doesnt_exist('System/Library/Frameworks/Security.framework', tree='SDKROOT')
project.add_file_if_doesnt_exist(pdf);

if project.modified:
    project.backup()
    project.saveFormat3_2()

print('done')
