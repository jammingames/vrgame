﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.Diagnostics;

public class XCodePosBuildProcess {

	private const string postProcessScriptPath = "Assets/Editor/xcode_post_process.py";

	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
		if (target == BuildTarget.iOS) {
			// Execute script to configure Xcode project
			Process p = new Process();
			p.StartInfo.FileName = "python";
			p.StartInfo.Arguments = string.Format(postProcessScriptPath + " \"{0}\" \"{1}\"", pathToBuiltProject, Application.dataPath + "/Ain2015DrivingStudy.pdf");
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardOutput = true;
			p.Start();
			UnityEngine.Debug.Log (p.StandardOutput.ReadToEnd());
			p.WaitForExit();
		}
	}

}

