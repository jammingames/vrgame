﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;




[RequireComponent(typeof (CarMovement))]
public class CarUserInput : MonoBehaviour
{
	private CarMovement m_Car; // the car controller we want to use
	
	
	private void Awake()
	{
		// get the car controller
		m_Car = GetComponent<CarMovement>();
	}
	
	
	private void FixedUpdate()
	{
		// pass the input to the car!
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		#if !MOBILE_INPUT
		float handbrake = CrossPlatformInputManager.GetAxis("Jump");
		m_Car.Move(h, v, 0, handbrake);
		
		#else
		m_Car.Move(h, 1, 0, 0f);
		float handbrake = 0;
		if (Input.GetMouseButtonDown(0))
		{
			handbrake = 1;
		}
		else if (!Input.GetMouseButtonDown(0))
		{
			handbrake = 0;
		}
		#endif
	}
}

