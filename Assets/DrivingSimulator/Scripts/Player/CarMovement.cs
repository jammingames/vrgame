﻿using UnityEngine;
using System.Collections;

public class CarMovement : MonoBehaviour {


	[SerializeField] private WheelCollider[] m_WheelColliders = new WheelCollider[4];
	[SerializeField] private GameObject[] m_WheelMeshes = new GameObject[4];
	[SerializeField] private float motorTorque;
	[SerializeField] private float brakeTorque;
	[SerializeField] private float topSpeed;
	[SerializeField] private float maxSteeringAngle;
	[SerializeField] private float maxHandbrakeTorque;
	[SerializeField] private float downForce;
	[SerializeField] private float slipLimit;
	[SerializeField] private Vector3 centreOfMassOffset;
	[SerializeField] private static int NoOfGears = 5;
	[SerializeField] private float m_RevRangeBoundary = 1f;


	private Rigidbody rb;
	private Quaternion[] wheelMeshLocalRotations;
	private float v, h, footbrake;
	private float currentTorque;
	private float steerAngle;
	private int m_GearNum;
	private float m_GearFactor;
	private float oldRotation;
	private const float k_ReversingThreshold = 0.01f;

	public float BrakeInput { get; private set; }
	public float CurrentSteerAngle{ get { return steerAngle; }}
	public float CurrentSpeed{ get { return rb.velocity.magnitude*2.23693629f; }}
	public float MaxSpeed{get { return topSpeed; }}
	public float Revs { get; private set; }
	public float AccelInput { get; private set; }


	void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	void Start()
	{
		wheelMeshLocalRotations = new Quaternion[4];
		for (int i = 0; i < 4; i++)
		{
			wheelMeshLocalRotations[i] = m_WheelMeshes[i].transform.localRotation;
		}
		m_WheelColliders[0].attachedRigidbody.centerOfMass = centreOfMassOffset;
		
		maxHandbrakeTorque = float.MaxValue;
		

		currentTorque = motorTorque - (1*motorTorque);;
	}



	public void Move(float steering, float accel, float footbrake, float handbrake)
	{
		for (int i = 0; i < 4; i++)
		{
			Quaternion quat;
			Vector3 position;
			m_WheelColliders[i].GetWorldPose(out position, out quat);
			m_WheelMeshes[i].transform.position = position;
			m_WheelMeshes[i].transform.rotation = quat;
		}
		
		//clamp input values
		steering = Mathf.Clamp(steering, -1, 1);
		AccelInput = accel = Mathf.Clamp(accel, 0, 1);
		BrakeInput = footbrake = -1*Mathf.Clamp(footbrake, -1, 0);
		handbrake = Mathf.Clamp(handbrake, 0, 1);
		
		//Set the steer on the front wheels.
		//Assuming that wheels 0 and 1 are the front wheels.
		steerAngle = steering*maxSteeringAngle;
		m_WheelColliders[0].steerAngle = steerAngle;
		m_WheelColliders[1].steerAngle = steerAngle;
		
		SteerHelper();
		ApplyDrive(accel, footbrake);
		CapSpeed();
		
		//Set the handbrake.
		//Assuming that wheels 2 and 3 are the rear wheels.
		if (handbrake > 0f)
		{
			var hbTorque = handbrake*maxHandbrakeTorque;
			m_WheelColliders[2].brakeTorque = hbTorque;
			m_WheelColliders[3].brakeTorque = hbTorque;
		}
		
		
		CalculateRevs();
		GearChanging();
		AddDownForce();
		//CheckForWheelSpin();
		TractionControl();
	}





	private void CapSpeed()
	{

		float speed = rb.velocity.magnitude;

			speed *= 3.6f;
			if (speed > topSpeed)
				rb.velocity = (topSpeed/3.6f) * rb.velocity.normalized;
	
	
	}

	private void ApplyDrive(float accel, float footbrake)
	{
		
		float thrustTorque;
	
	
			thrustTorque = accel * (currentTorque / 4f);
			for (int i = 0; i < 4; i++)
			{
				m_WheelColliders[i].motorTorque = thrustTorque;
			}
	
	
		
		for (int i = 0; i < 4; i++)
		{
			if (CurrentSpeed > 5 && Vector3.Angle(transform.forward, rb.velocity) < 50f)
			{
				m_WheelColliders[i].brakeTorque = brakeTorque*footbrake;
			}
			else if (footbrake > 0)
			{
				m_WheelColliders[i].brakeTorque = 0f;
				m_WheelColliders[i].motorTorque = -brakeTorque*footbrake;
			}		
		
	
		}
	}

	private void SteerHelper()
	{
		for (int i = 0; i < 4; i++)
		{
			WheelHit wheelhit;
			m_WheelColliders[i].GetGroundHit(out wheelhit);
			if (wheelhit.normal == Vector3.zero)
				return; // wheels arent on the ground so dont realign the rigidbody velocity
		}
		
		// this if is needed to avoid gimbal lock problems that will make the car suddenly shift direction
		if (Mathf.Abs(oldRotation - transform.eulerAngles.y) < 10f)
		{
			var turnadjust = (transform.eulerAngles.y - oldRotation) * 1;
			Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
			rb.velocity = velRotation * rb.velocity;
		}
		oldRotation = transform.eulerAngles.y;
	}


	// this is used to add more grip in relation to speed
	private void AddDownForce()
	{
		m_WheelColliders[0].attachedRigidbody.AddForce(-transform.up*downForce*
		                                               m_WheelColliders[0].attachedRigidbody.velocity.magnitude);
	}

	// crude traction control that reduces the power to wheel if the car is wheel spinning too much
	private void TractionControl()
	{
		WheelHit wheelHit;
	
			// loop through all wheels
			for (int i = 0; i < 4; i++)
			{
				m_WheelColliders[i].GetGroundHit(out wheelHit);
				
				AdjustTorque(wheelHit.forwardSlip);
			}
			
			
	
	}

	
	private void AdjustTorque(float forwardSlip)
	{
		if (forwardSlip >= slipLimit && currentTorque >= 0)
		{
			currentTorque -= 10;
		}
		else
		{
			currentTorque += 10;
			if (currentTorque > motorTorque)
			{
				currentTorque = motorTorque;
			}
		}
	}

	private void GearChanging()
	{
		float f = Mathf.Abs(CurrentSpeed/MaxSpeed);
		float upgearlimit = (1/(float) NoOfGears)*(m_GearNum + 1);
		float downgearlimit = (1/(float) NoOfGears)*m_GearNum;
		
		if (m_GearNum > 0 && f < downgearlimit)
		{
			m_GearNum--;
		}
		
		if (f > upgearlimit && (m_GearNum < (NoOfGears - 1)))
		{
			m_GearNum++;
		}
	}

	
	// simple function to add a curved bias towards 1 for a value in the 0-1 range
	private static float CurveFactor(float factor)
	{
		return 1 - (1 - factor)*(1 - factor);
	}
	
	
	// unclamped version of Lerp, to allow value to exceed the from-to range
	private static float ULerp(float from, float to, float value)
	{
		return (1.0f - value)*from + value*to;
	}
	
	
	private void CalculateGearFactor()
	{
		float f = (1/(float) NoOfGears);
		// gear factor is a normalised representation of the current speed within the current gear's range of speeds.
		// We smooth towards the 'target' gear factor, so that revs don't instantly snap up or down when changing gear.
		var targetGearFactor = Mathf.InverseLerp(f*m_GearNum, f*(m_GearNum + 1), Mathf.Abs(CurrentSpeed/MaxSpeed));
		m_GearFactor = Mathf.Lerp(m_GearFactor, targetGearFactor, Time.deltaTime*5f);
	}
	
	
	private void CalculateRevs()
	{
		// calculate engine revs (for display / sound)
		// (this is done in retrospect - revs are not used in force/power calculations)
		CalculateGearFactor();
		var gearNumFactor = m_GearNum/(float) NoOfGears;
		var revsRangeMin = ULerp(0f, m_RevRangeBoundary, CurveFactor(gearNumFactor));
		var revsRangeMax = ULerp(m_RevRangeBoundary, 1f, gearNumFactor);
		Revs = ULerp(revsRangeMin, revsRangeMax, m_GearFactor);
	}
	



}
