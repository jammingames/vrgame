﻿using UnityEngine;
using System.Collections;

public class DistanceCalculator : MonoBehaviour {

	public float distance;
	public float tick;
	Vector3 currentPos, lastPos;
	// Use this for initialization
	void Start () {

	}

	public void StartCheck()
	{
		currentPos = transform.position;
		StartCoroutine(DistanceCheck(tick));
		distance = 0;
	}
	
	IEnumerator DistanceCheck(float tick)
	{
		lastPos = currentPos;
		currentPos = transform.position;
		distance += Vector3.Distance(currentPos, lastPos);
		yield return new WaitForSeconds(tick);
		yield return StartCoroutine(DistanceCheck(tick));
	}

	public void StopCheck()
	{
		StopAllCoroutines();
	}
}
