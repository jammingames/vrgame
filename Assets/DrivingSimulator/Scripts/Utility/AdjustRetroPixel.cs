﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AlpacaSound;

public class AdjustRetroPixel : MonoBehaviour {

	#region variables
	public Vector2 minRes = new Vector2 (8, 8);
	public Vector2 maxRes = new Vector2(2048,1536);

	public float duration;

	public EaseType easer;

	Vector2 currentRes;
	Vector2 lastRes;

	float elapsedTime = 0;
	float currentTime = 0;

	bool shouldLerp = false;

	RetroPixel rp;

	Vector2 start;
	Vector2 range;
	Vector2 target;


	#endregion

	#region monobehaviours
	void Awake()
	{
		Init ();	
	}

	void Start()
	{

	}

	void OnEnable()
	{

	}

	void OnDisable()
	{

	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.J))
		{
			ChangeScale (minRes, duration, easer);
		}
		if (Input.GetKeyDown(KeyCode.U))
		{
			rp.horizontalResolution = (int)minRes.x;
			rp.verticalResolution = (int)minRes.y;
			ChangeScale (maxRes, duration, easer);
		}

		if (shouldLerp)
			LerpUpdate ();
	}

	void Init()
	{
		rp = GetComponent<RetroPixel>();
		currentRes = GetRes (rp);
		lastRes = currentRes;
		target = currentRes;
		shouldLerp = false;
		elapsedTime = 0;
		currentTime = 0;
	}
	#endregion

	#region eventHandlers



	#endregion

	#region public methods

	#region lerping
	public Vector2 GetRes(RetroPixel r)
	{
		return new Vector2 (r.horizontalResolution, r.verticalResolution);
	}

	public void ChangeScale (Vector2 target, float duration)
	{

		shouldLerp = true;
		if (lastRes != target) {
			rp.enabled = true;
			currentRes = GetRes (rp);
			start = currentRes;
			range = target - start;
			currentTime = 0;
			this.target = target;
			this.duration = duration;
			easer = EaseType.Linear;
			lastRes = currentRes;
		}

	}

	public void ChangeScale (Vector2 target, float duration, EaseType ease)
	{
		shouldLerp = true;
		if (lastRes != target) {
			rp.enabled = true;
			currentRes = GetRes (rp);
			start = currentRes;
			range = target - start;
			currentTime = 0;
			this.target = target;
			this.duration = duration;
			easer = ease;
			lastRes = currentRes;

		}
	}

	public void LerpUpdate()
	{

		currentRes = GetRes (rp);
		if (currentRes != target) {


			currentTime = Mathf.MoveTowards (currentTime, duration, Time.deltaTime);
			if (currentTime > duration) {
				currentTime = duration;
			}
			currentRes = (start + range * Ease.FromType (easer) (currentTime / duration));
			rp.horizontalResolution = (int)currentRes.x;
			rp.verticalResolution = (int)currentRes.y;

		}
		else
		{
			if (shouldLerp)
			{
//				rp.enabled = false;	
				shouldLerp = false;
			}
		}
		lastRes = currentRes;
	}
	#endregion

	#endregion


}
