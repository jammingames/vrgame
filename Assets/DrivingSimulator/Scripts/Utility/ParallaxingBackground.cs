﻿using UnityEngine;
using System.Collections;

public class ParallaxingBackground : MonoBehaviour {

	public float speed;
	public float imageWidth;
	public Camera cam;
	float limitsLeft, limitsRight;

	void Start()
	{
		imageWidth = GetComponentInChildren<SpriteRenderer>().bounds.size.x * 3;
		limitsRight = cam.ScreenToWorldPoint (new Vector3 (0, 0, 5)).x;
		limitsLeft = cam.ScreenToWorldPoint (new Vector3 (Screen.width, 0, 5)).x - (imageWidth-10);

		Vector3 pos = transform.position;
		Vector3 pos2 = pos;
		pos2.x = limitsLeft;
		pos.x = limitsRight;
		transform.position = pos;
//			RIGHT_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, 0, 5)).x;
//		LEFT_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 5)).x;
//		
//		BOT_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 5)).y;
//		TOP_SCREEN_EDGE = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 5)).y;
//

		StartCoroutine(transform.MoveTo(pos2, 60, ()=>{
			StartCoroutine(transform.MoveTo(pos, 60, null));
		}));
	}


}
