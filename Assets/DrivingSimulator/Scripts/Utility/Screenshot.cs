﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Screenshot : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.H))
		{
			StartCoroutine(TakeScreenshot());
		}
	}

	IEnumerator TakeScreenshot()
	{
				yield return new WaitForEndOfFrame ();


		string imageName = "testScreen" + Random.Range(0, 500).ToString();
		Application.CaptureScreenshot(Application.dataPath + "/../screenCaps/" + imageName + ".png");
//		// Create a texture the size of the screen, RGB24 format
//		int width = Screen.width;
//		int height = Screen.height;
//		Texture2D tex = new Texture2D (width, height, TextureFormat.RGB24, false);
//		// Read screen contents into the texture
//		tex.ReadPixels (new Rect (0, 0, width, height), 0, 0);
//		tex.Apply ();
//		yield return new WaitForEndOfFrame ();
//		
//		#if UNITY_EDITOR
//		
//		byte[] bytes = tex.EncodeToPNG ();
//		int imageCount = Directory.GetFiles (@Application.dataPath + "/../screenCaps", "*.png").Length + 1;
//		D.log(imageCount);
//		
//		File.WriteAllBytes (Application.dataPath + "/../screenCaps/testscreen-" + imageCount + ".png", bytes);
//		
//		#endif

	}
}
