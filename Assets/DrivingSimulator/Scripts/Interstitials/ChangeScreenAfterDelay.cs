﻿using UnityEngine;
using System.Collections;

public class ChangeScreenAfterDelay : MonoBehaviour
{
	public float delay;
	public Animator screen;
	
	IEnumerator Start ()
	{
		yield return StartCoroutine (Auto.Wait (delay));
		ScreenManager.instance.OpenPanel (screen);
	}
}