﻿var turnSpeed = 50;

function Update()
{
	
	if(Input.GetKey(KeyCode.LeftArrow))
	{
		transform.RotateAround(transform.position, transform.up, Time.deltaTime * turnSpeed);
	}
	if(Input.GetKey(KeyCode.RightArrow))
	{
		transform.RotateAround(transform.position, transform.up, Time.deltaTime * -turnSpeed);
	}
}