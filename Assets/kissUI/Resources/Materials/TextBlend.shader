// Shader created with Shader Forge v1.16 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.16;sub:START;pass:START;ps:flbk:Unlit/Transparent,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:False,rfrpn:Refraction,ufog:False,aust:True,igpj:True,qofs:1,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1300,x:36374,y:31753,varname:node_1300,prsc:2|emission-2612-OUT,alpha-1344-OUT;n:type:ShaderForge.SFN_Tex2d,id:1302,x:34030,y:31238,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_996,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ca327f3ff46b1984bba8e0a87a80162e,ntxv:0,isnm:False|UVIN-4261-UVOUT;n:type:ShaderForge.SFN_Subtract,id:1344,x:36074,y:32510,varname:node_1344,prsc:2|A-1658-OUT,B-5356-OUT;n:type:ShaderForge.SFN_Clamp01,id:1356,x:33806,y:30732,varname:node_1356,prsc:2|IN-1360-OUT;n:type:ShaderForge.SFN_Lerp,id:1360,x:33621,y:30732,varname:node_1360,prsc:2|A-1364-RGB,B-1366-RGB,T-3081-OUT;n:type:ShaderForge.SFN_Color,id:1364,x:33411,y:30722,ptovrint:False,ptlb:FillFrom,ptin:_FillFrom,varname:node_7836,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:1366,x:33245,y:30722,ptovrint:False,ptlb:FillTo,ptin:_FillTo,varname:node_5977,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_TexCoord,id:1370,x:32659,y:30891,varname:node_1370,prsc:2,uv:0;n:type:ShaderForge.SFN_Relay,id:1420,x:34616,y:30745,cmnt:red,varname:node_1420,prsc:2|IN-3619-OUT;n:type:ShaderForge.SFN_Relay,id:1422,x:34597,y:31255,cmnt:green,varname:node_1422,prsc:2|IN-3315-OUT;n:type:ShaderForge.SFN_Multiply,id:1424,x:34719,y:31188,varname:node_1424,prsc:2|A-1676-OUT,B-1422-OUT;n:type:ShaderForge.SFN_Color,id:1431,x:34520,y:31790,ptovrint:False,ptlb:Shadow,ptin:_Shadow,varname:node_303,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.00339757,c3:0.007352948,c4:1;n:type:ShaderForge.SFN_Relay,id:1433,x:34579,y:31713,cmnt:blue,varname:node_1433,prsc:2|IN-1302-B;n:type:ShaderForge.SFN_Multiply,id:1434,x:34715,y:31713,varname:node_1434,prsc:2|A-1431-RGB,B-1433-OUT;n:type:ShaderForge.SFN_Add,id:1519,x:35062,y:32067,cmnt:sum rgb,varname:node_1519,prsc:2|A-5122-OUT,B-5113-OUT,C-1553-OUT;n:type:ShaderForge.SFN_Multiply,id:1548,x:34719,y:31315,varname:node_1548,prsc:2|A-1422-OUT,B-1690-OUT;n:type:ShaderForge.SFN_Multiply,id:1553,x:34715,y:31849,varname:node_1553,prsc:2|A-1433-OUT,B-1431-A;n:type:ShaderForge.SFN_Multiply,id:1588,x:34720,y:30817,varname:node_1588,prsc:2|A-4015-OUT,B-1645-OUT;n:type:ShaderForge.SFN_Multiply,id:1627,x:34720,y:30691,varname:node_1627,prsc:2|A-1356-OUT,B-1420-OUT;n:type:ShaderForge.SFN_Lerp,id:1645,x:33806,y:30871,varname:node_1645,prsc:2|A-1364-A,B-1366-A,T-3081-OUT;n:type:ShaderForge.SFN_Subtract,id:1658,x:35853,y:32429,varname:node_1658,prsc:2|A-3235-OUT,B-5333-OUT;n:type:ShaderForge.SFN_Clamp01,id:1676,x:33806,y:31200,varname:node_1676,prsc:2|IN-1678-OUT;n:type:ShaderForge.SFN_Lerp,id:1678,x:33632,y:31200,varname:node_1678,prsc:2|A-1682-RGB,B-1684-RGB,T-3121-OUT;n:type:ShaderForge.SFN_Color,id:1682,x:33431,y:31178,ptovrint:False,ptlb:OutlineFrom,ptin:_OutlineFrom,varname:node_639,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.8344827,c3:1,c4:0;n:type:ShaderForge.SFN_Color,id:1684,x:33271,y:31178,ptovrint:False,ptlb:OutlineTo,ptin:_OutlineTo,varname:node_8423,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:1690,x:33806,y:31342,varname:node_1690,prsc:2|A-1682-A,B-1684-A,T-3121-OUT;n:type:ShaderForge.SFN_Clamp01,id:2023,x:34117,y:30092,varname:node_2023,prsc:2|IN-2025-OUT;n:type:ShaderForge.SFN_Lerp,id:2025,x:33946,y:30092,varname:node_2025,prsc:2|A-2029-RGB,B-2031-RGB,T-3065-OUT;n:type:ShaderForge.SFN_Color,id:2029,x:33571,y:30064,ptovrint:False,ptlb:BackgroundFrom,ptin:_BackgroundFrom,varname:node_229,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.99929,c2:1,c3:0.9926471,c4:1;n:type:ShaderForge.SFN_Color,id:2031,x:33411,y:30064,ptovrint:False,ptlb:BackgroundTo,ptin:_BackgroundTo,varname:node_7551,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1172414,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:2037,x:33946,y:30225,varname:node_2037,prsc:2|A-2029-A,B-2031-A,T-3065-OUT;n:type:ShaderForge.SFN_ToggleProperty,id:2072,x:34117,y:29977,ptovrint:False,ptlb:ShowBackground,ptin:_ShowBackground,varname:node_9340,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_If,id:2073,x:34334,y:30092,varname:node_2073,prsc:2|A-2072-OUT,B-2075-OUT,GT-2697-OUT,EQ-2023-OUT,LT-2697-OUT;n:type:ShaderForge.SFN_Vector1,id:2075,x:34117,y:30034,varname:node_2075,prsc:2,v1:1;n:type:ShaderForge.SFN_If,id:2197,x:35444,y:32199,cmnt:useBack,varname:node_2197,prsc:2|A-2072-OUT,B-2075-OUT,GT-3391-OUT,EQ-3563-OUT,LT-3391-OUT;n:type:ShaderForge.SFN_Add,id:2579,x:35062,y:32240,varname:node_2579,prsc:2|A-2037-OUT,B-3391-OUT;n:type:ShaderForge.SFN_Lerp,id:2610,x:35610,y:30749,varname:node_2610,prsc:2|A-2073-OUT,B-3898-OUT,T-5122-OUT;n:type:ShaderForge.SFN_Lerp,id:2611,x:35611,y:31158,varname:node_2611,prsc:2|A-2646-OUT,B-5095-OUT,T-5113-OUT;n:type:ShaderForge.SFN_Lerp,id:2612,x:35625,y:31647,varname:node_2612,prsc:2|A-4997-OUT,B-5106-OUT,T-1553-OUT;n:type:ShaderForge.SFN_Add,id:2646,x:35421,y:31158,varname:node_2646,prsc:2|A-2610-OUT,B-5095-OUT;n:type:ShaderForge.SFN_Add,id:2648,x:35255,y:31647,varname:node_2648,prsc:2|A-2611-OUT,B-5106-OUT;n:type:ShaderForge.SFN_Vector3,id:2697,x:34117,y:30225,varname:node_2697,prsc:2,v1:0,v2:0,v3:0;n:type:ShaderForge.SFN_ToggleProperty,id:2883,x:34052,y:30819,ptovrint:False,ptlb:hasRGBfx,ptin:_hasRGBfx,varname:node_1297,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_If,id:2884,x:34238,y:30819,varname:node_2884,prsc:2|A-2883-OUT,B-2885-OUT,GT-1302-A,EQ-1302-R,LT-1302-A;n:type:ShaderForge.SFN_Vector1,id:2885,x:34052,y:30881,varname:node_2885,prsc:2,v1:1;n:type:ShaderForge.SFN_Fmod,id:3004,x:33411,y:30232,varname:node_3004,prsc:2|A-4678-OUT,B-3005-OUT;n:type:ShaderForge.SFN_Divide,id:3005,x:33229,y:30377,varname:node_3005,prsc:2|A-3007-OUT,B-3054-OUT;n:type:ShaderForge.SFN_Vector1,id:3007,x:33053,y:30378,varname:node_3007,prsc:2,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:3054,x:33053,y:30453,ptovrint:False,ptlb:Lines,ptin:_Lines,varname:node_738,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:3065,x:33602,y:30232,varname:node_3065,prsc:2|A-3004-OUT,B-3054-OUT;n:type:ShaderForge.SFN_Multiply,id:3081,x:33436,y:30901,varname:node_3081,prsc:2|A-3083-OUT,B-3054-OUT;n:type:ShaderForge.SFN_Fmod,id:3083,x:33245,y:30901,varname:node_3083,prsc:2|A-4436-OUT,B-3087-OUT;n:type:ShaderForge.SFN_Divide,id:3087,x:33063,y:31027,varname:node_3087,prsc:2|A-3089-OUT,B-3054-OUT;n:type:ShaderForge.SFN_Vector1,id:3089,x:32877,y:31027,varname:node_3089,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:3121,x:33442,y:31352,varname:node_3121,prsc:2|A-3135-OUT,B-3054-OUT;n:type:ShaderForge.SFN_Fmod,id:3135,x:33243,y:31352,varname:node_3135,prsc:2|A-4651-OUT,B-3143-OUT;n:type:ShaderForge.SFN_Divide,id:3143,x:33048,y:31479,varname:node_3143,prsc:2|A-3163-OUT,B-3054-OUT;n:type:ShaderForge.SFN_Vector1,id:3163,x:32842,y:31479,varname:node_3163,prsc:2,v1:1;n:type:ShaderForge.SFN_Tex2d,id:3230,x:35206,y:32435,ptovrint:False,ptlb:Clip3,ptin:_Clip3,varname:node_9943,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Subtract,id:3235,x:35627,y:32326,varname:node_3235,prsc:2|A-2197-OUT,B-3272-OUT;n:type:ShaderForge.SFN_OneMinus,id:3272,x:35432,y:32420,varname:node_3272,prsc:2|IN-3230-A;n:type:ShaderForge.SFN_If,id:3315,x:34427,y:31244,varname:node_3315,prsc:2|A-2883-OUT,B-2885-OUT,GT-1302-A,EQ-1302-G,LT-1302-A;n:type:ShaderForge.SFN_Clamp01,id:3391,x:35222,y:32067,varname:node_3391,prsc:2|IN-1519-OUT;n:type:ShaderForge.SFN_Add,id:3447,x:34397,y:30819,varname:node_3447,prsc:2|A-2884-OUT,B-3315-OUT;n:type:ShaderForge.SFN_Clamp01,id:3563,x:35222,y:32240,varname:node_3563,prsc:2|IN-2579-OUT;n:type:ShaderForge.SFN_Vector1,id:3619,x:34430,y:30745,varname:node_3619,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:3891,x:35076,y:30762,varname:node_3891,prsc:2|A-1627-OUT,B-4214-OUT;n:type:ShaderForge.SFN_Clamp01,id:3898,x:35258,y:30762,varname:node_3898,prsc:2|IN-3891-OUT;n:type:ShaderForge.SFN_Clamp01,id:4015,x:34557,y:30819,varname:node_4015,prsc:2|IN-3447-OUT;n:type:ShaderForge.SFN_Multiply,id:4114,x:34263,y:31429,varname:node_4114,prsc:2|A-1302-B,B-4115-OUT;n:type:ShaderForge.SFN_Slider,id:4115,x:33935,y:31482,ptovrint:False,ptlb:BlendShadowWFill,ptin:_BlendShadowWFill,varname:node_7458,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:4;n:type:ShaderForge.SFN_Clamp01,id:4214,x:34427,y:31429,varname:node_4214,prsc:2|IN-4114-OUT;n:type:ShaderForge.SFN_TexCoord,id:4260,x:33480,y:31541,varname:node_4260,prsc:2,uv:0;n:type:ShaderForge.SFN_Rotator,id:4261,x:33691,y:31541,varname:node_4261,prsc:2|UVIN-4260-UVOUT,ANG-4274-OUT;n:type:ShaderForge.SFN_Vector1,id:4262,x:33282,y:31744,varname:node_4262,prsc:2,v1:57.29577;n:type:ShaderForge.SFN_ValueProperty,id:4273,x:33282,y:31687,ptovrint:False,ptlb:MainTexRotate,ptin:_MainTexRotate,varname:node_1732,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Divide,id:4274,x:33480,y:31687,varname:node_4274,prsc:2|A-4273-OUT,B-4262-OUT;n:type:ShaderForge.SFN_Rotator,id:4405,x:32877,y:30891,varname:node_4405,prsc:2|UVIN-1370-UVOUT,ANG-4407-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4406,x:32462,y:31048,ptovrint:False,ptlb:FillRotate,ptin:_FillRotate,varname:node_8952,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:180;n:type:ShaderForge.SFN_Divide,id:4407,x:32659,y:31037,varname:node_4407,prsc:2|A-4406-OUT,B-4409-OUT;n:type:ShaderForge.SFN_Vector1,id:4409,x:32462,y:31104,varname:node_4409,prsc:2,v1:57.29577;n:type:ShaderForge.SFN_ComponentMask,id:4436,x:33063,y:30891,varname:node_4436,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4405-UVOUT;n:type:ShaderForge.SFN_Vector1,id:4494,x:32472,y:31547,varname:node_4494,prsc:2,v1:57.29577;n:type:ShaderForge.SFN_Divide,id:4495,x:32657,y:31492,varname:node_4495,prsc:2|A-4497-OUT,B-4494-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4497,x:32472,y:31492,ptovrint:False,ptlb:OutlineRotate,ptin:_OutlineRotate,varname:node_3931,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:180;n:type:ShaderForge.SFN_Rotator,id:4508,x:32842,y:31342,varname:node_4508,prsc:2|UVIN-4516-UVOUT,ANG-4495-OUT;n:type:ShaderForge.SFN_TexCoord,id:4516,x:32657,y:31342,varname:node_4516,prsc:2,uv:0;n:type:ShaderForge.SFN_ComponentMask,id:4651,x:33048,y:31342,varname:node_4651,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4508-UVOUT;n:type:ShaderForge.SFN_Rotator,id:4666,x:33053,y:30232,varname:node_4666,prsc:2|UVIN-4671-UVOUT,ANG-4670-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4667,x:32671,y:30378,ptovrint:False,ptlb:BackgrndRotate,ptin:_BackgrndRotate,varname:node_4181,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Vector1,id:4669,x:32671,y:30437,varname:node_4669,prsc:2,v1:57.29577;n:type:ShaderForge.SFN_Divide,id:4670,x:32843,y:30378,varname:node_4670,prsc:2|A-4667-OUT,B-4669-OUT;n:type:ShaderForge.SFN_TexCoord,id:4671,x:32843,y:30232,varname:node_4671,prsc:2,uv:0;n:type:ShaderForge.SFN_ComponentMask,id:4678,x:33229,y:30232,varname:node_4678,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4666-UVOUT;n:type:ShaderForge.SFN_Clamp01,id:4997,x:35436,y:31647,varname:node_4997,prsc:2|IN-2648-OUT;n:type:ShaderForge.SFN_Clamp01,id:5095,x:34880,y:31188,varname:node_5095,prsc:2|IN-1424-OUT;n:type:ShaderForge.SFN_Clamp01,id:5106,x:34886,y:31713,varname:node_5106,prsc:2|IN-1434-OUT;n:type:ShaderForge.SFN_Clamp01,id:5113,x:34880,y:31315,varname:node_5113,prsc:2|IN-1548-OUT;n:type:ShaderForge.SFN_Clamp01,id:5122,x:34892,y:30817,varname:node_5122,prsc:2|IN-1588-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5315,x:34153,y:32837,ptovrint:False,ptlb:Right,ptin:_Right,varname:node_3094,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_TexCoord,id:5317,x:34307,y:32849,varname:node_5317,prsc:2,uv:0;n:type:ShaderForge.SFN_Step,id:5319,x:34504,y:32788,varname:node_5319,prsc:2|A-5317-U,B-5315-OUT;n:type:ShaderForge.SFN_Step,id:5321,x:34504,y:32909,varname:node_5321,prsc:2|A-5317-V,B-5335-OUT;n:type:ShaderForge.SFN_OneMinus,id:5323,x:34662,y:32788,varname:node_5323,prsc:2|IN-5319-OUT;n:type:ShaderForge.SFN_OneMinus,id:5325,x:34662,y:32909,varname:node_5325,prsc:2|IN-5321-OUT;n:type:ShaderForge.SFN_Subtract,id:5327,x:34852,y:32777,varname:node_5327,prsc:2|A-5329-OUT,B-5323-OUT;n:type:ShaderForge.SFN_Vector1,id:5329,x:34662,y:32732,varname:node_5329,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:5331,x:35029,y:32777,varname:node_5331,prsc:2|A-5327-OUT,B-5325-OUT;n:type:ShaderForge.SFN_OneMinus,id:5333,x:35190,y:32777,varname:node_5333,prsc:2|IN-5331-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5335,x:34153,y:32914,ptovrint:False,ptlb:Top,ptin:_Top,varname:node_8400,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:5338,x:34153,y:33186,ptovrint:False,ptlb:Left,ptin:_Left,varname:node_2520,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_TexCoord,id:5340,x:34307,y:33198,varname:node_5340,prsc:2,uv:0;n:type:ShaderForge.SFN_Step,id:5342,x:34504,y:33137,varname:node_5342,prsc:2|A-5340-U,B-5338-OUT;n:type:ShaderForge.SFN_Step,id:5344,x:34504,y:33258,varname:node_5344,prsc:2|A-5340-V,B-5358-OUT;n:type:ShaderForge.SFN_Subtract,id:5350,x:34852,y:33126,varname:node_5350,prsc:2|A-5352-OUT,B-5342-OUT;n:type:ShaderForge.SFN_Vector1,id:5352,x:34662,y:33081,varname:node_5352,prsc:2,v1:1;n:type:ShaderForge.SFN_Subtract,id:5354,x:35029,y:33126,varname:node_5354,prsc:2|A-5350-OUT,B-5344-OUT;n:type:ShaderForge.SFN_OneMinus,id:5356,x:35190,y:33126,varname:node_5356,prsc:2|IN-5354-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5358,x:34153,y:33263,ptovrint:False,ptlb:Bottom,ptin:_Bottom,varname:node_8070,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:1302-4273-2883-3054-1364-1366-4406-1682-1684-4497-1431-4115-2072-2029-2031-4667-3230-5315-5335-5338-5358;pass:END;sub:END;*/

Shader "kissUI/TextBlend" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _MainTexRotate ("MainTexRotate", Float ) = 0
        [MaterialToggle] _hasRGBfx ("hasRGBfx", Float ) = 0
        _Lines ("Lines", Float ) = 1
        _FillFrom ("FillFrom", Color) = (1,1,0,1)
        _FillTo ("FillTo", Color) = (1,0,0,1)
        _FillRotate ("FillRotate", Float ) = 180
        _OutlineFrom ("OutlineFrom", Color) = (0,0.8344827,1,0)
        _OutlineTo ("OutlineTo", Color) = (1,0,0,1)
        _OutlineRotate ("OutlineRotate", Float ) = 180
        _Shadow ("Shadow", Color) = (0,0.00339757,0.007352948,1)
        _BlendShadowWFill ("BlendShadowWFill", Range(0, 4)) = 0
        [MaterialToggle] _ShowBackground ("ShowBackground", Float ) = 0
        _BackgroundFrom ("BackgroundFrom", Color) = (0.99929,1,0.9926471,1)
        _BackgroundTo ("BackgroundTo", Color) = (0.1172414,0,1,1)
        _BackgrndRotate ("BackgrndRotate", Float ) = 0
        _Clip3 ("Clip3", 2D) = "white" {}
        _Right ("Right", Float ) = 1
        _Top ("Top", Float ) = 1
        _Left ("Left", Float ) = 0
        _Bottom ("Bottom", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent+1"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _FillFrom;
            uniform float4 _FillTo;
            uniform float4 _Shadow;
            uniform float4 _OutlineFrom;
            uniform float4 _OutlineTo;
            uniform float4 _BackgroundFrom;
            uniform float4 _BackgroundTo;
            uniform fixed _ShowBackground;
            uniform fixed _hasRGBfx;
            uniform float _Lines;
            uniform sampler2D _Clip3; uniform float4 _Clip3_ST;
            uniform float _BlendShadowWFill;
            uniform float _MainTexRotate;
            uniform float _FillRotate;
            uniform float _OutlineRotate;
            uniform float _BackgrndRotate;
            uniform float _Right;
            uniform float _Top;
            uniform float _Left;
            uniform float _Bottom;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float node_2075 = 1.0;
                float node_2073_if_leA = step(_ShowBackground,node_2075);
                float node_2073_if_leB = step(node_2075,_ShowBackground);
                float3 node_2697 = float3(0,0,0);
                float node_4666_ang = (_BackgrndRotate/57.29577);
                float node_4666_spd = 1.0;
                float node_4666_cos = cos(node_4666_spd*node_4666_ang);
                float node_4666_sin = sin(node_4666_spd*node_4666_ang);
                float2 node_4666_piv = float2(0.5,0.5);
                float2 node_4666 = (mul(i.uv0-node_4666_piv,float2x2( node_4666_cos, -node_4666_sin, node_4666_sin, node_4666_cos))+node_4666_piv);
                float node_3065 = (fmod(node_4666.g,(1.0/_Lines))*_Lines);
                float node_4405_ang = (_FillRotate/57.29577);
                float node_4405_spd = 1.0;
                float node_4405_cos = cos(node_4405_spd*node_4405_ang);
                float node_4405_sin = sin(node_4405_spd*node_4405_ang);
                float2 node_4405_piv = float2(0.5,0.5);
                float2 node_4405 = (mul(i.uv0-node_4405_piv,float2x2( node_4405_cos, -node_4405_sin, node_4405_sin, node_4405_cos))+node_4405_piv);
                float node_3081 = (fmod(node_4405.g,(1.0/_Lines))*_Lines);
                float node_4261_ang = (_MainTexRotate/57.29577);
                float node_4261_spd = 1.0;
                float node_4261_cos = cos(node_4261_spd*node_4261_ang);
                float node_4261_sin = sin(node_4261_spd*node_4261_ang);
                float2 node_4261_piv = float2(0.5,0.5);
                float2 node_4261 = (mul(i.uv0-node_4261_piv,float2x2( node_4261_cos, -node_4261_sin, node_4261_sin, node_4261_cos))+node_4261_piv);
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_4261, _MainTex));
                float node_2885 = 1.0;
                float node_2884_if_leA = step(_hasRGBfx,node_2885);
                float node_2884_if_leB = step(node_2885,_hasRGBfx);
                float node_3315_if_leA = step(_hasRGBfx,node_2885);
                float node_3315_if_leB = step(node_2885,_hasRGBfx);
                float node_3315 = lerp((node_3315_if_leA*_MainTex_var.a)+(node_3315_if_leB*_MainTex_var.a),_MainTex_var.g,node_3315_if_leA*node_3315_if_leB);
                float node_5122 = saturate((saturate((lerp((node_2884_if_leA*_MainTex_var.a)+(node_2884_if_leB*_MainTex_var.a),_MainTex_var.r,node_2884_if_leA*node_2884_if_leB)+node_3315))*lerp(_FillFrom.a,_FillTo.a,node_3081)));
                float node_4508_ang = (_OutlineRotate/57.29577);
                float node_4508_spd = 1.0;
                float node_4508_cos = cos(node_4508_spd*node_4508_ang);
                float node_4508_sin = sin(node_4508_spd*node_4508_ang);
                float2 node_4508_piv = float2(0.5,0.5);
                float2 node_4508 = (mul(i.uv0-node_4508_piv,float2x2( node_4508_cos, -node_4508_sin, node_4508_sin, node_4508_cos))+node_4508_piv);
                float node_3121 = (fmod(node_4508.g,(1.0/_Lines))*_Lines);
                float node_1422 = node_3315; // green
                float3 node_5095 = saturate((saturate(lerp(_OutlineFrom.rgb,_OutlineTo.rgb,node_3121))*node_1422));
                float node_5113 = saturate((node_1422*lerp(_OutlineFrom.a,_OutlineTo.a,node_3121)));
                float node_1433 = _MainTex_var.b; // blue
                float3 node_5106 = saturate((_Shadow.rgb*node_1433));
                float node_1553 = (node_1433*_Shadow.a);
                float3 emissive = lerp(saturate((lerp((lerp(lerp((node_2073_if_leA*node_2697)+(node_2073_if_leB*node_2697),saturate(lerp(_BackgroundFrom.rgb,_BackgroundTo.rgb,node_3065)),node_2073_if_leA*node_2073_if_leB),saturate(((saturate(lerp(_FillFrom.rgb,_FillTo.rgb,node_3081))*1.0)-saturate((_MainTex_var.b*_BlendShadowWFill)))),node_5122)+node_5095),node_5095,node_5113)+node_5106)),node_5106,node_1553);
                float3 finalColor = emissive;
                float node_2197_if_leA = step(_ShowBackground,node_2075);
                float node_2197_if_leB = step(node_2075,_ShowBackground);
                float node_3391 = saturate((node_5122+node_5113+node_1553));
                float4 _Clip3_var = tex2D(_Clip3,TRANSFORM_TEX(i.uv0, _Clip3));
                return fixed4(finalColor,(((lerp((node_2197_if_leA*node_3391)+(node_2197_if_leB*node_3391),saturate((lerp(_BackgroundFrom.a,_BackgroundTo.a,node_3065)+node_3391)),node_2197_if_leA*node_2197_if_leB)-(1.0 - _Clip3_var.a))-(1.0 - ((1.0-(1.0 - step(i.uv0.r,_Right)))-(1.0 - step(i.uv0.g,_Top)))))-(1.0 - ((1.0-step(i.uv0.r,_Left))-step(i.uv0.g,_Bottom)))));
            }
            ENDCG
        }
    }
    FallBack "Unlit/Transparent"
    //CustomEditor "ShaderForgeMaterialInspector"
}
