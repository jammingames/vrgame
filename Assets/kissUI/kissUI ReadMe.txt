///////////////////////////////////////
//	kissUI for Unity3D
//	by IzzySoft
//	version	0.1.5
///////////////////////////////////////

- About:
	A straightforward UI Design approach meant to Keep It Simple Silly*
	by adding Objects to the hierarchy in a Parent/Child node manner.

- Differences:
	* kissUI tries to take the less is more approach when possible. So you might
	not see very many Components attached to a GameObject for it to work.
	* kissUI tries to make the Layout process more relatable to say.. how HTML
	elements are aligned. Just the basics of course, keeping it simple.
	* kissUI uses the Parent to Hide/Clip/etc.. all the grand/children under it,
	with the ability to override that option on certain grand/children.
	
- Usage:
	Create a kiss Camera object as the root, and setup a Layer Mask.
	Add other kiss Objects underneath other kiss Objects as Nodes.
	
- Support:
	* Online Documentation:  http://kissui.izzysoft.com/docs/0.1.5/
	* Questions n Answers:  http://kissui.izzysoft.com/QnA/

- Changes:
	* check online at:	http://kissui.izzysoft.com/docs/0.1.5/
