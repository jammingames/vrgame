﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using kissUI;

[ExecuteInEditMode]
public class SiblingToFront : MonoBehaviour
{

	#region 1.Variables

	public List< kissImage > 	setFocused = new List< kissImage >();
	public bool					isFocused = false;
	public int					UIStyleInUse	= 0;
	public string[] ForegroundStyles = new string[]{ "Windows7", "MacOSX" };
	public string[] BackgroundStyles = new string[]{ "Windows7 Back", "MacOSX Back" };
	// --
	private List< kissImage >	all_images;
	private kissObject			this_ko;
	private kissRaycast			uiRaycast;
	private kissFocusGroup		this_FocusGroup;

	#endregion



	#region Unity Events

	void Start()
	{
		//Debug.Log( "SiblingToFront.Start()" );

		kissNode root_node = kissUtility.Find_kissUI_Root( this_ko.Node );
		if( root_node != null )
			uiRaycast = root_node.obj.GetComponent< kissRaycast >();

		if( uiRaycast != null && uiRaycast.FocusedGroupIndex < uiRaycast.FocusGroups.Count )
		{	
			kissFocusGroup active_fg = uiRaycast.FocusGroups[ uiRaycast.FocusedGroupIndex ];

			if( active_fg != this_FocusGroup )
				FocusGroup_OnFocusLost( this_FocusGroup );
		}
	}

	void OnEnable()
	{
		//Debug.Log( "SiblingToFront.OnEnable()" );

		this_ko = gameObject.GetComponent< kissObject >();
		this_ko.Node.OthersAdd( this );

		kissImage[] found_images = this_ko.GetComponentsInChildren< kissImage >();
		all_images = new List< kissImage >( found_images );

		for( int i = 0; i < all_images.Count; i++ )
		{
			if( all_images[ i ] == null )
				continue;
			
			all_images[ i ].OnMouseDown += Image_OnMouseDown;
		}

		kissObject.OnObjectCreated += Global_OnObjectCreated;
		kissObject.OnObjectDestroyed += Global_OnObjectDestroyed;
		kissObject.OnParentChanged += Global_OnParentChanged;

		this_FocusGroup = gameObject.GetComponent< kissFocusGroup >();

		if( this_FocusGroup != null )
		{
			this_FocusGroup.OnFocusReceived += FocusGroup_OnFocusReceived;
			this_FocusGroup.OnFocusLost += FocusGroup_OnFocusLost;
		}

		BaseStyle.BaseStyle_OnChanged += Style_OnChanged;
	}
	
	void OnDisable()
	{
		this_ko.Node.OthersRemove( this );

		for( int i = 0; i < all_images.Count; i++ )
		{
			if( all_images[ i ] == null )
				continue;
			
			all_images[ i ].OnMouseDown -= Image_OnMouseDown;
		}

		kissObject.OnObjectCreated -= Global_OnObjectCreated;
		kissObject.OnObjectDestroyed -= Global_OnObjectDestroyed;
		kissObject.OnParentChanged -= Global_OnParentChanged;

//		if( uiRaycast != null )
//		{
//			uiRaycast.FocusGroup_OnGroupChanged -= FocusGroup_OnGroupChanged;
//			uiRaycast.RemoveFocusGroup( this_FocusGroup );
//		}

		if( this_FocusGroup != null )
		{
			this_FocusGroup.OnFocusReceived -= FocusGroup_OnFocusReceived;
			this_FocusGroup.OnFocusLost -= FocusGroup_OnFocusLost;
		}

		BaseStyle.BaseStyle_OnChanged -= Style_OnChanged;
	}

	void OnDestroy()
	{
		//Debug.Log( "Destroyed!" );

		if( uiRaycast != null )
			uiRaycast.RemoveFocusGroup( this_FocusGroup );
	}

	#endregion



	#region kissUI Events

	void Image_OnMouseDown( kissImage img, kissMouseButton Btn, kissModifier KeyMod, Vector2 Pos )
	{
		//Debug.Log( "SiblingToFront.OnMouseDown()  " + img.name );

		//Debug.Log( "uiRaycast.SetActiveFocusGroupTo()    this_FocusGroup: " + this_FocusGroup );

		uiRaycast.SetActiveFocusGroupTo( this_FocusGroup );

		this_ko.SetSiblingToLast();
	}

	void Global_OnObjectCreated( kissObject ko )
	{
		if( ko.ObjectType != kissObjectType.Image )
			return;

		// is this new Image object Our Descendant? lets find out.
		kissObject found_ko = kissUtility.FindOtherCompInParents( ko, this );

		if( found_ko == this_ko )
		{
			//Debug.Log( "YES!   Newly created Image is our Grand/Child. Adding MouseDown event." );

			kissImage img = ko as kissImage;
			img.OnMouseDown += Image_OnMouseDown;
		}

	}

	void Global_OnObjectDestroyed( kissObject ko )
	{
		if( ko.ObjectType != kissObjectType.Image )
			return;
		
		
		kissObject found_ko = kissUtility.FindOtherCompInParents( ko, this );
		
		if( found_ko == this_ko )
		{
			//Debug.Log( "YES!   Just about to be Destroyed object is a Grand/Child of ours." );

			kissImage img = ko as kissImage;
			img.OnMouseDown -= Image_OnMouseDown;		// might not even need this, sinnce the image is about to be destroyed. :p
		}

	}

	void Global_OnParentChanged( kissObject ko, kissObject oldParent, kissObject newParent )
	{
		//Debug.Log( "S2F:  OnParentChanged()   Name: " + this.name );

		int ours = 0;
		int add = 0;
		int rem = 0;

		if( newParent != null )
		{
			if( newParent == this_ko )
			{
				//Debug.Log( "YES!   newParent is a Grand/Child of ours." );

				ours++;
				add++;
			}
			else
			{
				kissObject found_ko = kissUtility.FindOtherCompInParents( newParent, this );
				if( found_ko == this_ko )
				{
					//Debug.Log( "YES!   newParent is a Grand/Child of ours." );

					ours++;
					add++;
				}
				else
				{
					//Debug.Log( "No!   newParent is Not a Grand/Child of ours." );
				}
			}
		}

		if( oldParent != null )
		{
			if( oldParent == this_ko )
			{
				//Debug.Log( "YES!   oldParent is a Grand/Child of ours." );

				ours++;
				rem++;
			}
			else
			{
				kissObject found_ko = kissUtility.FindOtherCompInParents( oldParent, this );
				if( found_ko == this_ko  )
				{
					//Debug.Log( "YES!   oldParent is a Grand/Child of ours." );

					ours++;
					rem++;
				}
				else
				{
					//Debug.Log( "No!   oldParent is Not a Grand/Child of ours." );
				}
			}
		}


		if( ours == 2 )	{}	// No Change Needed.
		else
		{
			if( add > 0 )
				AddMouseDownHandlerToImageChildren( ko );

			if( rem > 0 )
				RemoveMouseDownHandlerToImageChildren( ko );
		}

	}

	void FocusGroup_OnFocusReceived( kissFocusGroup fg )
	{
		if( isFocused )
			return;

		//Debug.Log( "FocusGroup_OnFocusReceived()", fg );

		isFocused = true;

		for( int i = 0; i < setFocused.Count; i++ )
		{
			setFocused[ i ].ActiveState = kissState.Focused;
			setFocused[ i ].Update_State();
		}

		this_ko.SetSiblingToLast();

		if( UIStyleInUse < ForegroundStyles.Length )
			ChangeStylesTo( ForegroundStyles[ UIStyleInUse ] );
	}

	void FocusGroup_OnFocusLost( kissFocusGroup fg )
	{
		if( isFocused == false )
			return;

		//Debug.Log( "FocusGroup_OnFocusLost()", fg );

		isFocused = false;

		for( int i = 0; i < setFocused.Count; i++ )
		{
			setFocused[ i ].ActiveState = kissState.Normal;
			setFocused[ i ].Update_State();
		}

		if( UIStyleInUse < BackgroundStyles.Length )
			ChangeStylesTo( BackgroundStyles[ UIStyleInUse ] );
	}

	#endregion



	#region Other 1

	public void AddMouseDownHandlerToImageChildren( kissObject ko )
	{
		if( ko.ObjectType == kissObjectType.Image )
			AddMouseDownHandlerFor( ko as kissImage );

		for( int i = 0; i < ko.Node.Children.Count; i++ )
			AddMouseDownHandlerToImageChildren( ko.Node.Children[ i ].obj );
	}

	public void RemoveMouseDownHandlerToImageChildren( kissObject ko )
	{
		if( ko.ObjectType == kissObjectType.Image )
			RemoveMouseDownHandlerFor( ko as kissImage );
		
		for( int i = 0; i < ko.Node.Children.Count; i++ )
			RemoveMouseDownHandlerToImageChildren( ko.Node.Children[ i ].obj );
	}

	public void AddMouseDownHandlerFor( kissImage imgAddHandler )
	{
		bool isImageInListing = false;

		for( int i = 0; i < all_images.Count; i++ )
		{
			if( all_images[ i ] == null )
				continue;
			
			if( all_images[ i ].Tran == imgAddHandler.Tran )
			{
				all_images[ i ].OnMouseDown += Image_OnMouseDown;
				isImageInListing = true;
				break;
			}
		}

		if( isImageInListing == false )
		{
			imgAddHandler.OnMouseDown += Image_OnMouseDown;
			all_images.Add( imgAddHandler );
		}
	}

	public void RemoveMouseDownHandlerFor( kissImage imgRemoveHandler )
	{
		for( int i = 0; i < all_images.Count; i++ )
		{
			if( all_images[ i ] == null )
				continue;

			if( all_images[ i ].Tran == imgRemoveHandler.Tran )
			{
				all_images[ i ].OnMouseDown -= Image_OnMouseDown;
				break;
			}
		}
	}

	#endregion



	#region Styling

	void Style_OnChanged( string BaseDir )
	{
		for( int i = 0; i < ForegroundStyles.Length; i++ )
		{
			if( ForegroundStyles[ i ] == BaseDir )
			{
				UIStyleInUse = i;
				break;
			}
		}

		if( isFocused )
		{
			if( UIStyleInUse < ForegroundStyles.Length )
				ChangeStylesTo( ForegroundStyles[ UIStyleInUse ] );
		}
		else
		{
			if( UIStyleInUse < BackgroundStyles.Length )
				ChangeStylesTo( BackgroundStyles[ UIStyleInUse ] );
		}
	}

	public void ChangeStylesTo( string BaseDir )
	{
		kissFolderStruc UIStylesFolderStruc;
		string pathToUIStylesFile = "UIStyles/UIStyles";	// + ".txt"
		
		bool isGood = kissUtility.PopulateFolderStructure( pathToUIStylesFile, out UIStylesFolderStruc );
		
		if( isGood == false )
		{
			Debug.LogWarning( "Resources Folder Structure couldnt be loaded!  Aborting!", this );
			return;
		}

		ChangeStylesTo_Recursive( this_ko, UIStylesFolderStruc, BaseDir );
		
		this_ko.Refresh();
	}
	
	private void ChangeStylesTo_Recursive( kissObject ko, kissFolderStruc FS, string BaseDir )
	{
		if( ko.Style != null && ko.Style.AssetPath != "" )
		{
			string[] StylePath = ko.Style.AssetPath.Split( "/"[0] );
			StylePath[ 2 ] = BaseDir;
			string newPath2 = String.Join( "/", StylePath ).Replace( "Resources/", "").Replace( ".asset", "" );
			
			if( ko.ObjectType == kissObjectType.Camera )
			{
				kissStyleCamera camStyle = Resources.Load( newPath2 ) as kissStyleCamera;
				if( camStyle != null )
					(ko as kissCamera).Style = camStyle;
			}
			else if( ko.ObjectType == kissObjectType.Group )
			{
				kissStyleGroup grpStyle = Resources.Load( newPath2 ) as kissStyleGroup;
				if( grpStyle != null )
					(ko as kissGroup).Style = grpStyle;
			}
			else if( ko.ObjectType == kissObjectType.Layout )
			{
				kissStyleLayout loStyle = Resources.Load( newPath2 ) as kissStyleLayout;
				if( loStyle != null )
					(ko as kissLayout).Style = loStyle;
			}
			else if( ko.ObjectType == kissObjectType.Image )
			{
				kissStyleImage imgStyle = Resources.Load( newPath2 ) as kissStyleImage;
				if( imgStyle != null )
					(ko as kissImage).Style = imgStyle;
			}
		}
		
		for( int i = 0; i < ko.Node.Children.Count; i++ )
			ChangeStylesTo_Recursive( ko.Node.Children[ i ].obj, FS, BaseDir );
	}

	#endregion

}



