﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using kissUI;

public class EnableHoverOnDropTarget : MonoBehaviour
{
	public kissRaycast			uiRaycast;
	public List< kissImage >	dropTarget;
	// --
	private kissImage			imgSelf;
	private HitInfo				hi = null;

	void Start() {}		//added so Inspector shows Enable/Disable checkbox.

	public void onMouseDown()
	{
		if( uiRaycast == null )
		{
			Transform root_tran = kissUtility.Find_kissUI_Root( transform );
			uiRaycast = root_tran.GetComponent< kissRaycast >();
		}

		if( uiRaycast == null )
		{
			Debug.LogWarning( "uiRaycast not set for '" + this.name + "'.  Aborting!", this );
			return;
		}

		if( imgSelf == null )
			imgSelf = gameObject.GetComponent< kissImage >();
		
		hi = uiRaycast.GetHitInfo( imgSelf.Tran );
		
		if( hi == null )
			hi = uiRaycast.hitInfo;

		for( int i = 0; i < imgSelf.ClippedBy.Count; i++ )
		{
			if( imgSelf.ClippedBy[ i ].tran.name == "Content" )
				imgSelf.ClippedBy[ i ].enabled = false;
		}

		imgSelf.PosOffset = new Vector3( imgSelf.PosOffset.x, imgSelf.PosOffset.y, -10 );


		int missing_DropTargets = 0;

		for( int i = 0; i < dropTarget.Count; i++ )
		{
			kissImage img = dropTarget[ i ];

			if( img == null )
			{
				missing_DropTargets++;
				continue;
			}

			kissImageData hoverData = img.StateData[ (int) kissState.Hover ];

			if( hoverData != null )
				hoverData.isStateEnabled = true;
		}

		if( missing_DropTargets > 0 )
			Debug.LogWarning( "Warning:  Some Drop Targets seem to be missing! Can you update them?  (left click this Debug entry to ping the Drag Source that needs updating!)", imgSelf );
	}

	public void onMouseUp()
	{
		//Debug.Log( "onMouseUp()" );

		//bool isMouseOverDropTarget = hi.img_Hit == null ? false : hi.img_Hit.DragDropKind == kissImage.DragDropType.DropTarget;
		imgSelf.PosOffset = new Vector3( 0f, 0f, imgSelf.PosOffset.z );


		for( int i = 0; i < dropTarget.Count; i++ )
		{
			kissImage img = dropTarget[ i ];
			kissImageData hoverData = img.StateData[ (int) kissState.Hover ];

			if( hoverData != null )
			{
				hoverData.isStateEnabled = false;
				img.ActiveState = kissState.Normal;
				img.Update_State();
			}
		}
	}

}
