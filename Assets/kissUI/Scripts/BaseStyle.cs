﻿using UnityEngine;
using System;
using System.Collections;
using kissUI;

[ExecuteInEditMode]
public class BaseStyle : MonoBehaviour
{
	public kissObject StartingNode;
	public string[] UIStyles = new string[]{ "Windows7", "MacOSX" };
	//--
	//private kissObject this_ko;


	public delegate void del_BaseStyle_OnChanged( string BaseDir );
	public static del_BaseStyle_OnChanged BaseStyle_OnChanged = null;


	void OnEnable()
	{
		//this_ko = gameObject.GetComponent< kissObject >();

		//TODO:  Repopulate UIStyles array from "./Resouces/UIStyles" directory, exclude "Default".
	}

	void OnDisable()
	{
		//..
	}

	public void ChangeStylesTo( int StyleIndex )
	{
		if( StyleIndex < 0 || StyleIndex >= UIStyles.Length )
		{
			Debug.LogWarning( "BaseStyle.ChangeStylesTo()  StyleIndex has to be in Range!  Aborting.", this );
			return;
		}

		string BaseDir = UIStyles[ StyleIndex ];
		
		if( BaseStyle_OnChanged != null )
			BaseStyle_OnChanged( BaseDir );


//		kissFolderStruc UIStylesFolderStruc;
//		string pathToUIStylesFile = "UIStyles/UIStyles";	// + ".txt"
//
//		bool isGood = kissUtility.PopulateFolderStructure( pathToUIStylesFile, out UIStylesFolderStruc );
//
//		if( isGood == false )
//		{
//			Debug.LogWarning( "Resources Folder Structure couldnt be loaded!  Aborting!", this );
//			return;
//		}
//
//		ChangeStylesTo_Recursive( StartingNode, UIStylesFolderStruc, BaseDir );
//
//		StartingNode.Refresh();
	}

//	private void ChangeStylesTo_Recursive( kissObject ko, kissFolderStruc FS, string BaseDir )
//	{
//		if( ko.Style != null && ko.Style.AssetPath != "" )
//		{
//			string[] StylePath = ko.Style.AssetPath.Split( "/"[0] );
//			StylePath[ 2 ] = BaseDir;
//			string newPath2 = String.Join( "/", StylePath ).Replace( "Resources/", "").Replace( ".asset", "" );
//
//			if( ko.ObjectType == kissObjectType.Camera )
//			{
//				kissStyleCamera camStyle = Resources.Load( newPath2 ) as kissStyleCamera;
//				if( camStyle != null )
//					(ko as kissCamera).Style = camStyle;
//			}
//			else if( ko.ObjectType == kissObjectType.Group )
//			{
//				kissStyleGroup grpStyle = Resources.Load( newPath2 ) as kissStyleGroup;
//				if( grpStyle != null )
//					(ko as kissGroup).Style = grpStyle;
//			}
//			else if( ko.ObjectType == kissObjectType.Layout )
//			{
//				kissStyleLayout loStyle = Resources.Load( newPath2 ) as kissStyleLayout;
//				if( loStyle != null )
//					(ko as kissLayout).Style = loStyle;
//			}
//			else if( ko.ObjectType == kissObjectType.Image )
//			{
//				kissStyleImage imgStyle = Resources.Load( newPath2 ) as kissStyleImage;
//				if( imgStyle != null )
//					(ko as kissImage).Style = imgStyle;
//			}
//		}
//
//		for( int i = 0; i < ko.Node.Children.Count; i++ )
//		{
//			kissNode child_node = ko.Node.Children[ i ];
//			kissObject child_ko = child_node.obj;
//
//			ChangeStylesTo_Recursive( child_ko, FS, BaseDir );
//		}
//
//	}

}






