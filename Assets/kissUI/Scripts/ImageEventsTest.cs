﻿using UnityEngine;
using System.Collections;
using kissUI;

[ExecuteInEditMode]
public class ImageEventsTest : MonoBehaviour
{
	kissImage img;
	
	// Use this for initialization
	//void Start(){}
	
	// Update is called once per frame
	//void Update(){}
	
	void OnEnable()
	{
		//Debug.Log( "ImageEventsTest:  OnEnable()" );
		
		if( img == null )
			img = gameObject.GetComponent< kissImage >();
		
		if( img != null )
			img.OnSizeChanged += onImageResized;
	}
	
	void OnDisable()
	{
		//Debug.Log( "ImageEventsTest:  OnDisable()" );
		
		if( img != null )
			img.OnSizeChanged -= onImageResized;
	}
	
	void onImageResized()
	{
		//Debug.Log( "ImageEventsTest:  onImageResized()" );
	}
}
