﻿using UnityEngine;
using System.Collections;
using kissUI;

[ExecuteInEditMode]
public class ReparentSourceOnMouseDrop : MonoBehaviour
{
	kissImage img = null;


	// ---- Unity Events ----

	void OnEnable()
	{
		img = GetComponent< kissImage >();
		
		if( img != null )
		{
			img.OnMouseDrop += onMouseDrop;
		}
	}
	
	void OnDisable()
	{
		if( img != null )
		{
			img.OnMouseDrop -= onMouseDrop;
		}
	}
	
	
	// ---- kissUI Events ----
	
	public void onMouseDrop( kissImage img, kissMouseButton Btn, kissModifier KeyMod, Vector2 Pos, kissImage imgSource )
	{
		//Debug.Log( "ReparentSourceOnMouseDrop:   onMouseDrop()", img );

		SiblingToFront stf = imgSource.GetComponentInParent< SiblingToFront >();
		if( stf != null )
			stf.RemoveMouseDownHandlerFor( imgSource );

		imgSource.PosOffset = new Vector3( 0f, 0f, -.1f );
		imgSource.ParentTo( img );

		stf = imgSource.GetComponentInParent< SiblingToFront >();
		if( stf != null )
		{
			stf.AddMouseDownHandlerFor( imgSource );
			kissObject stf_ko = stf.GetComponent< kissObject >();

			if( stf_ko != null )
				stf_ko.SetSiblingToLast();
		}
		
	}

	bool IsDragSourceOurs( kissImage imgSource )
	{
		bool isDragSourceOurs = false;

		EnableHoverOnDropTarget ehodt = imgSource.GetComponent< EnableHoverOnDropTarget >();
		
		if( ehodt == null )
			return isDragSourceOurs;
		
		for( int i = 0; i < ehodt.dropTarget.Count; i++ )
		{
			if( img.Tran == ehodt.dropTarget[ i ].Tran )
			{
				isDragSourceOurs = true;
				break;
			}
		}

		return isDragSourceOurs;
	}
	
}
