﻿using UnityEngine;
using System.Collections;
using kissUI;

[ExecuteInEditMode]
public class AnimOffsetsForZone : MonoBehaviour
{
	public kissImage.SliceZone ZoneArea;
	public float XAnim = 0.01f;
	public float YAnim = 0.00f;
	
	private kissImage _img;
	private kissImageZone _imgZone;
	private Renderer _imgZoneRend;
	private Vector2 v2_NewOffset = new Vector2();
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateRefrences();
		
		if( _imgZoneRend != null )
		{
			Material mat = null;
			if( Application.isEditor )
				mat = _imgZoneRend.sharedMaterial;
			else
				mat = _imgZoneRend.material;
			
			if( mat != null )
			{
				v2_NewOffset.x += XAnim;
				v2_NewOffset.y += YAnim;
				mat.SetTextureOffset( "_MainTex", v2_NewOffset );
			}
		}
	}
	
	void UpdateRefrences()
	{
		if( _img == null )
			_img = this.GetComponent< kissImage >();
		
		if( _img != null )
		{
			if( _imgZone == null || _imgZone.sliceZone != ZoneArea )
			{
				_imgZone = _img.sliceZones[ (int) ZoneArea ];
				
				if( _imgZone != null )
				{
					_imgZoneRend = _imgZone.GetComponent< Renderer >();
				}
			}
		}
	}
}
